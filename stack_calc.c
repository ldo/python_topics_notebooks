/*
    Example shared library written to use to demonstrate how to call a
    variadic function written in C typesafely from Python using
    ctypes.

    Copyright 2024 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
    Licensed CC0 <https://creativecommons.org/publicdomain/zero/1.0/>;
    do with it what you will.
*/

#include <stdbool.h>
#include <iso646.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include "stack_calc.h"

static void bugcheck
  (
    bool cond,
    const char * msg,
    ...
  )
  {
    if (not cond)
      {
        va_list args;
        vfprintf(stderr, msg, args);
        va_end(args);
        fprintf(stderr, "\n");
        abort();
      } /*if*/
  } /*bugcheck*/

real stack_calc
  (
    stackcalc_op op,
    ...
  )
  /* starting with an empty operand stack, carries out the specified
    operations on the specified operands, at the end of which there
    should be just a single value on the operand stack. This value is
    popped and returned as the function result. */
  {
    real * opstack = NULL;
    size_t
        opstacklen = 0,
        opstackalloc = 0;
    real result;
    va_list args;
    va_start(args, op);
    bool first = true;
    bool done = false;
    for (;;)
      {
        if (not first)
          {
            op = va_arg(args, stackcalc_op);
          } /*if*/
        first = false;
        switch (op)
          {
        case STACKCALC_END:
            done = true;
        break;
        case STACKCALC_PUSH:
              {
                if (opstacklen == opstackalloc)
                  {
                    opstackalloc += 16;
                    opstack = realloc(opstack, opstackalloc * sizeof(real));
                    bugcheck(opstack != NULL, "unable to expand operand stack beyond %d", opstacklen);
                  } /*if*/
                opstack[opstacklen] = va_arg(args, real);
                ++opstacklen;
              }
        break;
        case STACKCALC_ADD:
        case STACKCALC_SUB:
        case STACKCALC_MUL:
        case STACKCALC_DIV:
            bugcheck(opstacklen >= 2, "not enough operands for operator %d", op);
              {
                const real op2 = opstack[opstacklen - 1];
                real op1 = opstack[opstacklen - 2];
                switch (op)
                  {
                case STACKCALC_ADD:
                    op1 += op2;
                break;
                case STACKCALC_SUB:
                    op1 -= op2;
                break;
                case STACKCALC_MUL:
                    op1 *= op2;
                break;
                case STACKCALC_DIV:
                    op1 /= op2;
                break;
                  } /*switch*/
                --opstacklen;
                opstack[opstacklen - 1] = op1;
              }
        break;
        default:
            bugcheck(false, "invalid operator %d", op);
        break;
          } /*switch*/
        if (done)
            break;
      } /*for*/
    va_end(args);
    bugcheck(opstacklen == 1, "expecting exactly one result left on stack");
    result = opstack[0];
    free(opstack);
    return result;
  } /*stack_calc*/
