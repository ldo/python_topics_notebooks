{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `ctypes` Tips #\n",
    "\n",
    "[`ctypes`](https://docs.python.org/3/library/ctypes.html) is a very handy tool for building Python wrappers for shared libraries written for C or C++. In most cases, it is probably preferable to use this, rather than write an *extension module* in C or C++ to provide the Python API: it can take a lot of code to implement the necessary C/C++ wrappers to represent Python objects and methods, while this can usually be done directly in Python with a fraction of the effort.\n",
    "\n",
    "While the documentation for `ctypes` is quite comprehensive, there are a few subtle points that might not be clear.\n",
    "\n",
    "A Python wrapper will typically need a lot of things from the `ctypes` module. Its own documentation page uses wildcard imports in the examples, which I prefer to avoid. Instead, I reference its exports by importing the module under a shorter name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ctypes as ct"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load The Runtime Library, Not The Development Library ##\n",
    "\n",
    "Consider the following directory entries currently on my Debian system for the [Cairo](https://cairographics.org/) graphics library:\n",
    "\n",
    "    /usr/lib/x86_64-linux-gnu/libcairo.so -> libcairo.so.2.11600.0\n",
    "    /usr/lib/x86_64-linux-gnu/libcairo.so.2 -> libcairo.so.2.11600.0\n",
    "    /usr/lib/x86_64-linux-gnu/libcairo.so.2.11600.0\n",
    "\n",
    "As you can see, there are 3 separate names for the same file. Which one should you use?\n",
    "\n",
    "The answer is, use the name `libcairo.so.2`. The unversioned name comes from the *development* package:\n",
    "\n",
    "    > dpkg-query -S /usr/lib/x86_64-linux-gnu/libcairo.so\n",
    "    libcairo2-dev:amd64: /usr/lib/x86_64-linux-gnu/libcairo.so\n",
    "\n",
    "while the versioned names come from the *runtime* package:\n",
    "\n",
    "    > dpkg-query -S /usr/lib/x86_64-linux-gnu/libcairo.so.2\n",
    "    libcairo2:amd64: /usr/lib/x86_64-linux-gnu/libcairo.so.2\n",
    "\n",
    "So, in a wrapper for Cairo, you would load the library using something like\n",
    "\n",
    "    cairo = ct.cdll.LoadLibrary(\"libcairo.so.2\")\n",
    "\n",
    "You only need to care about the first numeric component of the version, since that is the one incremented for any ABI changes (which might necessitate changes to your wrapper).\n",
    "\n",
    "While having the development package installed is useful while you are developing your wrapper (being able to refer to the include files for information, etc), you should only require your users to have the runtime package in order to be able to run scripts that use your wrapper. Of course, they, too might find the development package useful when writing such scripts. But let that be their choice.\n",
    "\n",
    "This only applies to distros like Debian which publish their packages in precompiled binary form. In ones like Gentoo, where users install everything from source, there is no distinction between “development” and “runtime” packages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pointer Tips ##\n",
    "\n",
    "Most pointer types (all the ones constructed with `POINTER()`) are subclasses of `_Pointer` and share some common properties. The exceptions are `c_void_p` and `c_char_p`, discussed separately below.\n",
    "\n",
    "These common pointer types have a `contents` attribute you can use to dereference the pointer. This returns a reference to the `ctypes` object holding the value, and can be assigned to to point it to a different object. Non-pointer `ctypes` types in turn have a `value` attribute that you can use to access or change the value.\n",
    "\n",
    "Alternatively, you can index the pointer as though it were an array to directly access the value pointed to, so `p[0]` is equivalent to `p.contents.value`, and this works for both getting the current value and assigning a new value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i1 = ct.c_int(3)\n",
    "p1 = ct.pointer(i1)\n",
    "p2 = ct.POINTER(ct.c_int)(ct.c_int(3))\n",
    "print(p1.contents, p1.contents.value, p2.contents.value, p2[0])\n",
    "p2[0] = 5\n",
    "print(p1.contents, p2.contents)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that dereferencing a `NULL` pointer (whether via indexing or by looking at the `contents` attribute) is not allowed (though you can assign to the `contents` attribute of a `NULL` pointer to make it non-`NULL`). Interestingly, you cannot seem to assign a `NULL` value to an existing pointer object—at least, not directly.\n",
    "\n",
    "You can check for a `NULL` pointer by treating the pointer as a `bool`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "p3 = ct.POINTER(ct.c_int)()\n",
    "\n",
    "for p, name in ((p1, \"p1\"), (p2, \"p2\"), (p3, \"p3\")) :\n",
    "    sys.stdout.write(name + \" \")\n",
    "    if bool(p) :\n",
    "        sys.stdout.write(\"has contents %d\" % p[0])\n",
    "    else :\n",
    "        sys.stdout.write(\"is NULL\")\n",
    "    #end if\n",
    "    sys.stdout.write(\"\\n\")\n",
    "#end for"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `contents` attribute can be used to selectively access parts of the value being pointed to, without copying the whole thing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MyStruct(ct.Structure) :\n",
    "    _fields_ = \\\n",
    "        [\n",
    "            (\"field1\", ct.c_int),\n",
    "            (\"field2\", ct.c_int),\n",
    "        ]\n",
    "#end MyStruct\n",
    "\n",
    "p1 = ct.POINTER(MyStruct)(MyStruct(4, 5))\n",
    "p2 = ct.POINTER(MyStruct)(p1.contents) # sharing same struct\n",
    "\n",
    "print(\"before:\", p2.contents.field1, p2.contents.field2)\n",
    "p1.contents.field1 = 6\n",
    "print(\"after:\", p2.contents.field1, p2.contents.field2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `c_void_p` ##\n",
    "\n",
    "The `ctypes` explanation of `c_void_p` (the untyped pointer) is that the Python type is `int` or `None`.  When creating a `c_void_p`, you can pass an integer for the address (including 0 for `NULL`), or you can pass `None` as an alternative for `NULL`. But when getting back one of these, the 0 or `NULL` address is always converted to `None`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p1 = ct.c_void_p(3)\n",
    "p2 = ct.c_void_p(0)\n",
    "print(p1.value, p2.value)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that `c_void_p` has no `contents` attribute for pointer dereferencing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting Field Offsets ##\n",
    "\n",
    "For some reason, the documentation page doesn’t currently mention any equivalent of the C `offsetof` construct, even though `ctypes` does support this. You get it via the `offset` attribute of a field, itself accessed as an attribute of the structure definition. You can also get the field size in a similar way, e.g."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Fields1(ct.Structure) :\n",
    "    _fields_ = \\\n",
    "        [\n",
    "            (\"field1\", ct.c_int),\n",
    "            (\"field2\", ct.c_int),\n",
    "        ]\n",
    "#end Fields1\n",
    "\n",
    "class Fields2(ct.Structure) :\n",
    "    _fields_ = \\\n",
    "        [\n",
    "            (\"field1\", ct.c_int),\n",
    "            (\"field2\", ct.c_double),\n",
    "        ]\n",
    "#end Fields2\n",
    "\n",
    "Fields1.field2.offset, Fields1.field2.size, Fields2.field2.offset, Fields2.field2.size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting Addresses Of Python Objects ##\n",
    "\n",
    "Sometimes you want to pass the address of the data inside a Python object directly to a library routine, to save copying data back and forth. This is particularly useful for Python objects of type `bytes` and `bytearray`, as well as arrays created with the [`array`](https://docs.python.org/3/library/array.html) module. This has to be done in slightly different ways for these different objects.\n",
    "\n",
    "To demonstrate this, I will make calls to the low-level `libc` [`memcpy`(3)](https://linux.die.net/man/3/memcpy) routine to copy data between Python objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "libc = ct.cdll.LoadLibrary(\"libc.so.6\")\n",
    "libc.memcpy.restype = ct.c_void_p\n",
    "libc.memcpy.argtypes = (ct.c_void_p, ct.c_void_p, ct.c_size_t) # dst, src, count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a `bytes` object, a simple `cast` is sufficient to obtain the address of the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b1 = b\"some:text\"\n",
    "b2 = b\"other text\"\n",
    "print(b1, b2)\n",
    "b1adr = ct.cast(b1, ct.c_void_p).value\n",
    "b2adr = ct.cast(b2, ct.c_void_p).value\n",
    "libc.memcpy(b2adr, b1adr, 5)\n",
    "print(b1, b2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a `bytearray`, things are slightly more involved."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b1 = bytearray(b\"different text\")\n",
    "b1adr = ct.addressof((ct.c_ubyte * len(b1)).from_buffer(b1))\n",
    "libc.memcpy(b2adr, b1adr, 6)\n",
    "print(b1, b2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By the way, you can’t use this technique on `bytes`; it appears this only works on *mutable* objects.\n",
    "\n",
    "[`array`](https://docs.python.org/3/library/array.html) arrays have a `buffer_info()` method which returns the address and length of the underlying memory buffer. While this still works, it is apparently deprecated. So the same trick works as for `bytearray`s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import array\n",
    "b1 = array.array(\"B\", b\"yet other text\")\n",
    "b1adr = ct.addressof((ct.c_ubyte * len(b1)).from_buffer(b1))\n",
    "libc.memcpy(b2adr, b1adr, 7)\n",
    "print(b1.tobytes(), b2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Casting can be used to create a pointer to a `ctypes` array type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = bytearray(b\"some text\")\n",
    "b1 = (ct.c_ubyte * 0).from_buffer(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, I have set the array length to 0, which prevents me from using `b1` directly to access any of the bytes in `b`, but a pointer constructed from `b1` is not so constrained:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = ct.cast(b1, ct.POINTER(ct.c_ubyte))\n",
    "[chr(c) for c in p[0:3]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because the original Python object is mutable, `ctypes` allows me to use the pointer to assign to its components from within Python (this would not be allowed for a pointer into a `bytes` object, for example):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p[5] = ord(\"z\")\n",
    "b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, external libraries are not going to respect Python’s access-control mechanisms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Avoiding Buffer Copying ##\n",
    "\n",
    "The [`struct`](https://docs.python.org/3/library/struct.html) module is useful for constructing and pulling apart low-level structures, if you don’t mind a certain amount of copying. For example, the [`os`](https://docs.python.org/3/library/os.html) module provides `readv` and `writev` methods to allow directly reading streams of bytes into one or more separate input buffers (“scatter-read”) and directly writing the contents of one or more separate output buffers into a single stream (“gather-write”) without first having to concatenate them together, and it would be nice to be able to take advantage of this.\n",
    "\n",
    "What may not be clear from the docs is that the “bytes-like objects” mentioned can be `ctypes` objects: these implement the necessary “buffer protocol”, so you can read directly into them and write directly out from them. Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import struct\n",
    "\n",
    "class FixedLine(ct.Structure) :\n",
    "    \"simulate layout of an 80-column punch card.\"\n",
    "    _fields_ = \\\n",
    "        [\n",
    "            (\"stmt_nr\", 5 * ct.c_char),\n",
    "            (\"contd\", ct.c_char),\n",
    "            (\"stmt\", 66 * ct.c_char),\n",
    "            (\"seq_nr\", 8 * ct.c_char),\n",
    "        ]\n",
    "\n",
    "    def __str__(self) :\n",
    "        return \\\n",
    "            str(tuple(getattr(self, f[0]) for f in self._fields_))\n",
    "    #end __str__\n",
    "#end FixedLine\n",
    "\n",
    "print(\"Length of FixedLine = %d\" % ct.sizeof(FixedLine)) # should be 80\n",
    "\n",
    "tmpfile = os.open(\"/tmp\", os.O_RDWR | os.O_TMPFILE)\n",
    "\n",
    "#+\n",
    "# ctypes structure out, struct.unpack in\n",
    "#-\n",
    "\n",
    "print(\"ctypes structure out, struct.unpack in\")\n",
    "\n",
    "os.writev \\\n",
    "  (\n",
    "    tmpfile,\n",
    "    [\n",
    "        FixedLine(b\"  100\", b\" \", b\"PRINT 'HI THERE'\", b\"00000100\"),\n",
    "        FixedLine(b\"     \", b\" \", b\"GO TO 100\", b\"00000200\"),\n",
    "    ]\n",
    "  )\n",
    "\n",
    "print(\"bytes written: %d\" % os.lseek(tmpfile, 0, os.SEEK_CUR))\n",
    "\n",
    "fixed_line_fmt = \"5s1s66s8s\"\n",
    "fixed_line_len = struct.calcsize(fixed_line_fmt)\n",
    "\n",
    "print(\"length of fixed_line struct = %s\" % fixed_line_len) # should be 80\n",
    "\n",
    "os.lseek(tmpfile, 0, os.SEEK_SET)\n",
    "\n",
    "while True :\n",
    "    line = os.read(tmpfile, fixed_line_len)\n",
    "    if line == b\"\" :\n",
    "        break\n",
    "    print(struct.unpack(fixed_line_fmt, line))\n",
    "#end while\n",
    "\n",
    "#+\n",
    "# The other way: struct.pack out, ctypes structure in\n",
    "#-\n",
    "\n",
    "print(\"The other way: struct.pack out, ctypes structure in\")\n",
    "\n",
    "os.lseek(tmpfile, 0, os.SEEK_SET)\n",
    "\n",
    "os.ftruncate(tmpfile, 0)\n",
    "\n",
    "os.writev \\\n",
    "  (\n",
    "    tmpfile,\n",
    "    [\n",
    "        struct.pack(fixed_line_fmt, b\"  200\", b\" \", b\"PRINT 'HI AGAIN'\", b\"00001100\"),\n",
    "        struct.pack(fixed_line_fmt, b\"     \", b\" \", b\"GO TO 200\", b\"00001200\"),\n",
    "    ]\n",
    "  )\n",
    "\n",
    "print(\"bytes written: %d\" % os.lseek(tmpfile, 0, os.SEEK_CUR))\n",
    "\n",
    "os.lseek(tmpfile, 0, os.SEEK_SET)\n",
    "\n",
    "buf = FixedLine()\n",
    "while True :\n",
    "    nrbytes = os.readv(tmpfile, [buf])\n",
    "    if nrbytes == 0 :\n",
    "        break\n",
    "    print(nrbytes, buf)\n",
    "#end while\n",
    "\n",
    "os.close(tmpfile)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compatibility Between `struct` And `ctypes` ###\n",
    "\n",
    "Can you intermix use of `struct` and `ctypes` facilities? That is, use them to operate directly on the same in-memory data, without any copying of low-level buffers back and forth?\n",
    "\n",
    "Yes you can, provided you use the `struct` functions in “native-endian” mode, and you are sufficiently careful about alignment requirements that `ctypes` imposes (using the “`@`” prefix in the `struct` format prefix will take both of these into account). Here is an example of how to use `struct.unpack` on a `ctypes` structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = Fields2(35, 1 / 3)\n",
    "\n",
    "print(struct.unpack(\"@id\", f))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is probably of limited use, since you can pull the fields directly out of the `ctypes` structure using `ctypes`’ own facilities.\n",
    "\n",
    "Going the other way is also doable: it would involve using `struct.pack` to turn a sequence of Python objects into a byte buffer, and then constructing a `ctypes` pointer of a suitable struct type into that buffer to access the fields. This is probably even of less use, though."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `c_char` And `c_char_p` ##\n",
    "\n",
    "A `c_char_p` is not quite equivalent to `ct.POINTER(c_char)`; it is assumed to point to a *null-terminated* array of `c_char`. Accessing the `value` attribute returns the data up to, but not including, the terminating null:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = b\"hello\\0 there\"\n",
    "ct.cast(b, ct.c_char_p).value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note you cannot assign to the `value` or `contents` of a `c_char_p` (this silently reallocates the buffer to hold the new value):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ct.cast(b, ct.c_char_p).contents = b\"text\"\n",
    "b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But you can to the `value` of an _array_ of `c_char` (note the extra null inserted after the value):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ct.cast(b, ct.POINTER(len(b) * ct.c_char))[0][0:4] = (4 * ct.c_char)(*list(b\"text\"))\n",
    "b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here’s a similar thing done to a `bytearray` instead of a `bytes` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = bytearray(b\"hello\\0 there\")\n",
    "(len(b) * ct.c_char).from_buffer(b).value = b\"tex\"\n",
    "b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array Conversions ##\n",
    "\n",
    "Conversion of a ctypes array (at least of simple element types) to a Python sequence is quite straightforward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c_arr = (3 * ct.c_int)(5, 4, 3)\n",
    "list(c_arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Conversion the other way is slightly more involved:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arr = [8, 7, 6]\n",
    "c_arr = (len(arr) * ct.c_int)(*arr)\n",
    "c_arr, list(c_arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calling Varargs Routines ##\n",
    "\n",
    "Some C routines take variable numbers of arguments. `ctypes` does not directly support calling such routines with differing numbers of arguments, but it is possible to do so by dynamically constructing an appropriately-typed entry-point object, with suitable conversions from the types of the Python objects passed as actual arguments. Again, `ctypes` does not provide direct access to the class constructor for doing this, but it can be done by cloning an existing entry point.\n",
    "\n",
    "For example, consider the [`snprintf`(3)](https://manpages.debian.org/1/snprintf.3.html) function, which has a prototype that looks like this:\n",
    "\n",
    "    int snprintf(char str[restrict .size], size_t size,\n",
    "               const char *restrict format, ...);\n",
    "\n",
    "Sure, this is not something you would normally want to call from Python, given that the [`%`-operator](https://docs.python.org/3/library/stdtypes.html#old-string-formatting) already gives you access to the full range of `printf`-style formatting features, and more. But dealing with this routine helps to illustrate the general technique.\n",
    "\n",
    "We can define the fixed part of the prototype like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "libc.snprintf.restype = ct.c_int\n",
    "libc.snprintf.argtypes = (ct.c_char_p, ct.c_size_t, ct.c_char_p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but clearly that is not sufficient to actually make calls to this routine. However, using that prototype as a starting point, here is a Python wrapper that facilitates making such calls, by dynamically constructing the argument types for a copy of the entry point:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typemappings = \\\n",
    "    { # mapping from Python types to ctypes types\n",
    "        int : ct.c_int,\n",
    "        float : ct.c_double,\n",
    "        # add more if you like, but more complex types\n",
    "        # will likely require special handling\n",
    "    }\n",
    "\n",
    "def snprintf(strsize, format, vals) :\n",
    "    \"invokes snprintf with the specified format and parameters, returning\" \\\n",
    "    \" a string of up to strsize bytes.\"\n",
    "    if not all(type(v) in typemappings for v in vals) :\n",
    "        raise TypeError(\"unsupported type present in vals\")\n",
    "        # todo: could give more detail about which element(s) of vals are at fault\n",
    "    #end if\n",
    "    result = bytes((0,) * (strsize + 1))\n",
    "    basefunc = libc.snprintf\n",
    "      # fixed part of type info already set up\n",
    "    func = type(basefunc).from_address(ct.addressof(basefunc))\n",
    "      # same entry point address, but can have entirely different arg/result types\n",
    "    func.restype = basefunc.restype # keep same result type\n",
    "    all_arg_types = list(basefunc.argtypes) # start with fixed part of arglist\n",
    "    c_format = format.encode()\n",
    "    all_args = [result, strsize + 1, c_format] # actual args for fixed part of arglist\n",
    "    for val in vals : # construct rest of arglist\n",
    "        cvaltype = typemappings[type(val)]\n",
    "        all_args.append(cvaltype(val)) # converted argument value\n",
    "        all_arg_types.append(cvaltype) # corresponding converted argument type\n",
    "    #end for\n",
    "    func.argtypes = all_arg_types\n",
    "    result_len = func(*all_args) # includes trailing null iff output is truncated\n",
    "    return result[:min(result_len, strsize)].decode() # exclude trailing null\n",
    "#end snprintf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A sample call to this routine might look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1\n",
    "b = 2\n",
    "snprintf(25, \"The sum of %d and %d is %d.\", [a, b, a + b])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`printf` functions can also accept strings. Some other varargs functions may want a trailing `NULL` argument to indicate the end of the argument list. Dealing with both of these cases is left as an exercise for the reader."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: Wrapper For Cairo ##\n",
    "\n",
    "In order to offer specific examples, the following discussion will refer to details of the implementation of **Qahirah**, my Python wrapper for the [**Cairo**](http://cairographics.org/) graphics library. This can be obtained from [GitLab](https://gitlab.com/ldo/qahirah) or [GitHub](https://github.com/ldo/qahirah)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reference-Counted Objects ##\n",
    "\n",
    "Some libraries have their own systems for keeping track of reference counts of objects. It’s probably easier to avoid library objects having a reference count greater than 1; just let Python itself keep track of references to your Python wrapper object, and have your `__del__()` method dispose of the library object. Then the library object automatically exists for as long as your Python wrapper object exists."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Round-Trip Object Mapping ##\n",
    "\n",
    "The underlying library may implement several different types of objects, and objects of one type may hold references to objects of another type. There will likely be API calls not only to set such references to particular objects, but also to retrieve the current object references from the referencing objects. How do you map this back to Python?\n",
    "\n",
    "For example, in Cairo, a drawing `Context` can have a current _source_ for computing rendered pixel values, which is a reference to a `Pattern`. You have a `set_source` call to set the source to a specified `Pattern`, and you have a `get_source` call to retrieve a reference to the last-set `Pattern`.\n",
    "\n",
    "It is easy enough to wrap the `set_source` call in an equivalent Python method on the `Context` wrapper, which takes a `Pattern` wrapper and extracts and passes both underlying objects to the library call. But what about `get_source`? Ideally, if you already have a Python wrapper object for that `Pattern`, it would be good to return that, rather than create a new wrapper.\n",
    "\n",
    "One way to do this would be to have a separate field in your `Context` wrapper which saves the last-set `Pattern` wrapper when the `set_source` method is called, then your `get_source` method can “cheat” and, rather than calling the Cairo routine at all, it simply returns the value of this field. This has some consequences I find undesirable:\n",
    "* it means the wrapper object has to be kept around, even if the user doesn’t need it.\n",
    "* it stands the risk of getting out of sync, if the user makes a direct call to the Cairo `set_source` routine via some other way.\n",
    "\n",
    "There is a more elegant solution, where you keep a class variable in the referenced class (`Pattern`, in this case) which holds a mapping from underlying object references to the corresponding Python objects; then the `get_source` method can simply look up the returned value in this dictionary; if an entry already exists, it can return that existing wrapper, otherwise it can automatically construct a new wrapper, add it to the dictionary, and return it.\n",
    "\n",
    "A further twist is to make this dictionary a `WeakValueDictionary`. That way, entries in it only exist for as long as the caller has other references to those Python wrapper objects; if the user gets rid of their own references, then they disappear from this mapping as well. This keeps memory usage for unwanted objects from growing without bounds.\n",
    "\n",
    "In order to make this work, the constructor for the wrapper object has to be implemented entirely within the `__new__()` method, rather than using `__init__()`. This way, you can control whether a new wrapper object is created or not.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constructors Versus Create Methods ##\n",
    "\n",
    "Related to the previous topic, I find it best to reserve the constructors for my wrapper classes for internal use, namely to construct wrapper objects around already-created library objects. Then I provide a separate set of `create()` methods (typically defined as `classmethod`s), which do the creation of library objects and call the constructor internally as appropriate, returning the newly-created wrapper for the newly-created library object. This way, the constructor centralizes all the mechanism for mapping underlying library objects to corresponding wrapper objects.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Avoiding Segfaults With Transient Python Objects ##\n",
    "\n",
    "There are certain situations where you might construct a `ctypes` object on the fly, directly in a call to a low-level library routine. This can mean that the only reference to the object disappears, and the object is reclaimed, before the library routine gets a chance to use it.\n",
    "\n",
    "I have found this can happen particularly with callbacks. For example:\n",
    "\n",
    "    callback_type = ct.CFUNCTYPE(...)\n",
    "\n",
    "    def my_callback(...) :\n",
    "        ...\n",
    "    #end my_callback\n",
    "\n",
    "    result = library.libroutine(..., callback_type(my_callback), ...)\n",
    "\n",
    "It is best to avoid this. Instead, keep the callback wrapper constructed by `callback_type()` in a Python variable for at least the duration of the library call:\n",
    "\n",
    "    my_callback_wrapper = callback_type(my_callback)\n",
    "    result = library.libroutine(..., my_callback_wrapper, ...)\n",
    "\n",
    "Further than this, you might be installing the callback for later use, which means the library might try to call it after the function in which `my_callback_wrapper` was defined exits. In this situation, you will need to keep the wrapper object somewhere more permanent, such as an instance variable, where it will stay in existence as long as it is needed:\n",
    "\n",
    "    self._my_callback_wrapper = callback_type(my_callback)\n",
    "    result = library.install_callback(..., self._my_callback_wrapper, ...)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Avoiding Segfaults On Program Exit ##\n",
    "\n",
    "There is a drawback with custom `__del__()` methods on Python objects, which surfaces at program exit time: **there are no guarantees about the order in which they are called at exit time**. For example, if the underlying library object has already been deleted, then attempts to call disposal routines can cause intermittent segfaults. Of course, at times other than program termination, `__del__()` cleanup should continue to work OK.\n",
    "\n",
    "The answer is for your wrapper module to install an `atexit` cleanup routine which **deletes the `__del__()` methods from all your classes that have them**. At the time that `atexit` routines are called, the state of things is still guaranteed to be sane; they will only go to pieces afterwards. For example, in my Cairo wrapper above, among the last few lines of code is something like\n",
    "\n",
    "    def _atexit() :\n",
    "        for cls in Context, Surface, Device, Pattern, ... etc ... :\n",
    "            delattr(cls, \"__del__\")\n",
    "        #end for\n",
    "    #end _atexit\n",
    "    atexit.register(_atexit)\n",
    "\n",
    "Now, any of these objects that are disposed after this will no longer try to call any `__del__()` methods, which should be OK because everything is going away anyway."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
