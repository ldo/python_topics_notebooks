{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# `ctypes` Tip Extra — Calling A Variadic Function\n",
    "\n",
    "This is an additional [`ctypes`](https://docs.python.org/3/library/ctypes.html)-related notebook. It is separated out from the main “`ctypes` Tips” one because it requires the accompanying C code in `stack_calc.[ch]` to be built into a shared library before it can run. Copy the shared library into the directory containing this notebook before proceeding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ctypes as ct\n",
    "\n",
    "stack_calc_lib = ct.CDLL(\"./stack_calc.so\")\n",
    "\n",
    "# If you got here, that means the library was successfully loaded."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Variadic_ functions are functions in C which can take a variable number of arguments of possibly variable type. In the example shared library, there is a function called `stack_calc`, which is declared like this:\n",
    "\n",
    "```\n",
    "real stack_calc\n",
    "  (\n",
    "    stackcalc_op op, /* first operator */\n",
    "    ...\n",
    "      /* remaining operands (of type real)/operators (of type stackcalc_op),\n",
    "        ending with STACKCALC_END */\n",
    "  );\n",
    "```\n",
    "\n",
    "This function implements a simple stack-based calculator, and its argument list represents a sequence of arithmetic operations (and accompanying operand values, where appropriate) to perform, after which the final calculated value is returned as the function result.\n",
    "\n",
    "The C function expects its argument list to consist of sequences of items as follows:\n",
    "\n",
    "* `STACKCALC_END` — marks the end of the argument list. There must be just one item left on the operand stack, which is returned as the function result.\n",
    "* `STACKCALC_PUSH` — must be followed by a real value, which is pushed on the operand stack.\n",
    "* `STACKCALC_ADD`, `STACKCALC_SUB`, `STACKCALC_MUL`, `STACKCALC_DIV` — perform the specified operation on the top two items on the operand stack, popping them and pushing the result.\n",
    "\n",
    "In the Python version, we won’t need the caller to end the argument list with `STACKCALC_END`, since Python already knows how long the list is. Also, we can dispense with an explicit `STACKCALC_PUSH`, since if we see a real argument, we can assume that is meant to be pushed as an operand.\n",
    "\n",
    "First, let us replicate the necessary types from the C source to allow communication between it and our Python code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import enum\n",
    "\n",
    "# following must be consistent with definitions in C code\n",
    "\n",
    "real = ct.c_double\n",
    "stackcalc_op = ct.c_uint\n",
    "\n",
    "class STACKCALC(enum.IntEnum) :\n",
    "    \"Python equivalent of the C enum.\"\n",
    "    END = 0 # marks end of arglist\n",
    "    PUSH = 1\n",
    "      # followed by a real operand in the arglist, which is\n",
    "      # pushed on the operand stack\n",
    "  # following are not followed by operands in the arglist;\n",
    "  # instead they expect two operands on the stack, which are\n",
    "  # popped, have the specified operation applied to them, and\n",
    "  # the result pushed back on the stack.\n",
    "    ADD = 2\n",
    "    SUB = 3\n",
    "    MUL = 4\n",
    "    DIV = 5\n",
    "\n",
    "#end STACKCALC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual, the function entry point needs to have argument and result types defined. `ctypes` doesn’t directly support variadic arguments, so we define the `argtypes` to just include any fixed arguments that might come before the variable part. In this case, there is nothing, because in our Python wrapper we will treat the initial operator as just another part of the variable argument list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stack_calc_lib.stack_calc.restype = real\n",
    "stack_calc_lib.stack_calc.argtypes = ()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us define our Python wrapper function, also called `stack_calc`. The way this works is, it will make a copy of the `stack_calc_lib.stack_calc` entry-point object, and adjust its argtypes list to match a dynamically-constructed argument list, based on the values the user passed.\n",
    "\n",
    "**Note the use** of argument types `ct.c_uint` and `ct.c_double`, instead of `stackcalc_op` and `real` respectively; this is because of the rules for passing arguments to variadic functions in C, where integers smaller than `int` or `unsigned int` get promoted to one of those two, and `float` values get promoted to `double`. So I always use the latter types, regardless of how the types specific to this function are defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def stack_calc(*args) :\n",
    "    basefunc = stack_calc_lib.stack_calc\n",
    "    func = type(basefunc).from_address(ct.addressof(basefunc))\n",
    "    func.restype = basefunc.restype\n",
    "    all_arg_types = []\n",
    "    all_args = []\n",
    "    for item in args :\n",
    "        if isinstance(item, (real, float)) :\n",
    "            all_args.extend([STACKCALC.PUSH, item])\n",
    "            all_arg_types.extend([stackcalc_op, ct.c_double])\n",
    "        elif isinstance(item, (int, STACKCALC)) :\n",
    "            op = STACKCALC(item)\n",
    "            if op not in (STACKCALC.ADD, STACKCALC.SUB, STACKCALC.MUL, STACKCALC.DIV) :\n",
    "                raise ValueError(\"invalid operator %s\" % op)\n",
    "            #end if\n",
    "            all_args.append(op)\n",
    "            all_arg_types.append(ct.c_uint)\n",
    "        else :\n",
    "            raise TypeError(\"expecting another operator or operand: %s\" % repr(item))\n",
    "        #end if\n",
    "    #end for\n",
    "    all_args.append(STACKCALC.END)\n",
    "    all_arg_types.append(stackcalc_op)\n",
    "    func.argtypes = all_arg_types\n",
    "    result = func(*all_args)\n",
    "    return result\n",
    "#end stack_calc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us try calling this function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"(3.0 + 4.0) × 5.0 =\", stack_calc(3.0, 4.0, STACKCALC.ADD, 5.0, STACKCALC.MUL))\n",
    "print(\"3.0 + 4.0 × 5.0 =\", stack_calc(3.0, 4.0, 5.0, STACKCALC.MUL, STACKCALC.ADD))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a more complex example of calling this function, let’s try implementing a square-root approximation using Newton-Raphson iteration. This function will compute the requisite series of terms to perform the approximation to the specified number of steps:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sqrt_approx(x : float, nr_steps : int) :\n",
    "    \"returns an argument list suitable for passing to stack_calc.\"\n",
    "    expr = (x / 2,) # initial approximation\n",
    "    for i in range(nr_steps) :\n",
    "        expr = expr + (x,) + expr + (STACKCALC.DIV, STACKCALC.ADD, 2.0, STACKCALC.DIV)\n",
    "    #end for\n",
    "    return expr\n",
    "#end sqrt_approx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let’s try using it to compute some successive approximations to the square root of 2:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for n in range(5) :\n",
    "    expr = sqrt_approx(2.0, n)\n",
    "    print(\"sqrt(2)[%d] %s = %.5f\" % (len(expr), repr(expr), stack_calc(*expr)))\n",
    "#end for"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, this is a good algorithm with fast (second-order) convergence. But a corresponding combinatorial explosion in terms means things can get pretty long-winded pretty quickly. This could be shortened by avoiding needless repetition, if our stack machine had additional operators like “duplicate the top element” and “exchange the top two elements”. But I’ll leave the implementation of those as an exercise for the reader."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "Calling such a variadic function from C itself can be tricky, because there is very limited type checking that the C compiler can do on such calls. However, our Python wrapper can perform its own very thorough type checking, allowing the caller of our wrapper to use it safely; passing the wrong types will trigger the usual Python exceptions, allowing such errors to be noticed and fixed quickly.\n",
    "\n",
    "Using the function correctly, avoiding stack underflow or leftover result operands, however, is another matter ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
