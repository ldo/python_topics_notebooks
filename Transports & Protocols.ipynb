{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Transports & Protocols #\n",
    "\n",
    "The [`asyncio` event loop](https://docs.python.org/3/library/asyncio-eventloop.html#opening-network-connections) offers three ways of managing asynchronous network connections:\n",
    "\n",
    "* at the lowest level, directly doing byte-oriented I/O on [socket objects](https://docs.python.org/3/library/asyncio-eventloop.html#working-with-socket-objects-directly).\n",
    "* at a higher level, but still doing byte-oriented I/O, via its [streams](https://docs.python.org/3/library/asyncio-stream.html) mechanism.\n",
    "* at a more advanced level, via [*Transports and Protocols*](https://docs.python.org/3/library/asyncio-protocol.html); these allow you to define custom message formats. The documentation says this offers the highest performance, but it can take more work to set up.\n",
    "\n",
    "The third one is the one we will be talking about here. The two main object classes involved are\n",
    "* *Transports* — manage the transmission and reception of bytes of data over the connection.\n",
    "* *Protocols* — manage the interpretation of _incoming_ bytes as meaningful message data.\n",
    "\n",
    "The two concepts are closely tied together. You cannot meaningfully instantiate a transport without giving it a protocol to handle the data. Conversely, the transport will call the protocol’s [`connection_made()`](https://docs.python.org/3/library/asyncio-protocol.html#asyncio.BaseProtocol.connection_made) method once communication with the other end is functional, which the latter can use to save a reference to the transport object that it can use for *sending* data.\n",
    "\n",
    "How a protocol communicates with the calling program — notifying it of incoming decoded messages, and accepting outgoing messages to be encoded and sent out via the transport — are not specified as part of the base transport and protocol classes. These mechanisms can be defined in whatever way the protocol implementation sees fit. For example, the actual encoding of outgoing messages might be done via some code elsewhere in the calling program, which then calls the transport to send it directly, without involving the protocol object at all.\n",
    "\n",
    "## Example Protocol ##\n",
    "\n",
    "To try to make things clearer, let us start by implementing a custom Protocol. This will be a very simple one, which just sends and receives decimal integers, one to a line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "import socket\n",
    "import asyncio"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class NumberStream(asyncio.Protocol) :\n",
    "    \"parses the incoming data stream into a sequence of integers, one to a line.\" \\\n",
    "    \" Also provides the send_number() method, which will send an integer in the same\" \\\n",
    "    \" format.\"\n",
    "\n",
    "    def __init__(self) :\n",
    "        super().__init__()\n",
    "        self.curline = \"\"\n",
    "        self.numbers = []\n",
    "        self.awaiting = None\n",
    "        self.eof = False\n",
    "        self.transport = None\n",
    "    #end __init__\n",
    "\n",
    "    # Methods defined by Protocol interface:\n",
    "    # (I implement just the bare minimum of these, enough for the\n",
    "    # example to work)\n",
    "\n",
    "    def connection_made(self, transport) :\n",
    "        # Save transport object for use to send outgoing messages.\n",
    "        self.transport = transport\n",
    "    #end connection_made\n",
    "\n",
    "    def connection_lost(self, exc) :\n",
    "        self.transport = None\n",
    "    #end connection_lost\n",
    "\n",
    "    def data_received(self, data) :\n",
    "        sys.stderr.write(\"NumberStream %s: data received: %s\\n\" % (repr(self), repr(data))) # debug\n",
    "        data = self.curline + data.decode()\n",
    "        while True :\n",
    "            data = data.split(\"\\n\", 1)\n",
    "            if len(data) == 1 :\n",
    "                self.curline, = data\n",
    "                break\n",
    "            #end if\n",
    "            curnum, data = data\n",
    "            self.numbers.append(int(curnum))\n",
    "            if self.awaiting != None and not self.awaiting.done() :\n",
    "                # wake up any waiters\n",
    "                self.awaiting.set_result(None)\n",
    "            #end if\n",
    "        #end while\n",
    "    #end data_received\n",
    "\n",
    "    def eof_received(self) :\n",
    "        sys.stderr.write(\"NumberStream %s: eof received\\n\" % repr(self)) # debug\n",
    "        self.eof = True\n",
    "        if self.awaiting != None :\n",
    "            self.awaiting.set_exception(StopAsyncIteration(\"transport EOF\"))\n",
    "        #end if\n",
    "    #end eof_received\n",
    "\n",
    "    # Application-specific methods for caller use:\n",
    "\n",
    "    async def get_next_number(self) :\n",
    "        \"for caller’s use to retrieve incoming decoded numbers.\"\n",
    "        while True :\n",
    "            if len(self.numbers) != 0 :\n",
    "                result = self.numbers.pop(0)\n",
    "                break\n",
    "            #end if\n",
    "            if self.eof :\n",
    "                raise StopAsyncIteration(\"transport EOF\")\n",
    "            #end if\n",
    "            awaiting = None\n",
    "            if self.awaiting == None :\n",
    "                awaiting = asyncio.get_running_loop().create_future()\n",
    "                self.awaiting = awaiting\n",
    "            #end if\n",
    "            await self.awaiting\n",
    "            if awaiting == self.awaiting :\n",
    "                self.awaiting = None # I created it, I get rid of it\n",
    "            #end if\n",
    "        #end while\n",
    "        return result\n",
    "    #end get_next_number\n",
    "\n",
    "    def send_number(self, num) :\n",
    "        \"for caller’s use to encode and send outgoing numbers.\"\n",
    "        data = (str(num) + \"\\n\").encode()\n",
    "        self.transport.write(data)\n",
    "    #end send_number\n",
    "\n",
    "#end NumberStream"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start with, we will use a standard transport type: client and server will communicate via an `AF_UNIX` socket with the following name. Note that the leading null means the name exists in Linux’s [abstract socket namespace](https://manpages.debian.org/7/unix.en.html#Abstract_sockets), so no actual filename needs to be created."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NUMBER_SERVER = b\"\\x00Number Server\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the code for the server task. All this server does is receive a number, add 1 to it, and send the result back.\n",
    "\n",
    "Note that the [`create_unix_server`](https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.create_unix_server) call returns a [`Server`](https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.Server) object, which is not actually very useful. For example, it provides no (public) way to retrieve the associated protocol instance. To get this, you must provide a protocol factory which does its own instantiation of the protocol class and stashes the result somewhere before returning a copy of it.\n",
    "\n",
    "Note also that the protocol object is not created immediately, so it is returned via a future that the main loop can await."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "async def run_server() :\n",
    "\n",
    "    number_stream = asyncio.get_running_loop().create_future()\n",
    "\n",
    "    def create_number_stream() :\n",
    "        sys.stderr.write(\"server creating number stream\\n\") # debug\n",
    "        proto = NumberStream()\n",
    "        number_stream.set_result(proto)\n",
    "        return proto\n",
    "    #end create_number_stream\n",
    "\n",
    "    server = await asyncio.get_running_loop().create_unix_server \\\n",
    "      (\n",
    "        protocol_factory = create_number_stream,\n",
    "        path = NUMBER_SERVER\n",
    "      )\n",
    "    number_stream = await number_stream\n",
    "    while not number_stream.eof :\n",
    "        try :\n",
    "            num = await number_stream.get_next_number()\n",
    "        except StopAsyncIteration :\n",
    "            break\n",
    "        #end try\n",
    "        sys.stderr.write(\"Server got number %d, sending back %d\\n\" % (num, num + 1))\n",
    "        number_stream.send_number(num + 1)\n",
    "    #end while\n",
    "    server.close()\n",
    "#end run_server\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a client task that opens a connection to the server, sends it some numbers, gets the results back, and closes the connection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "async def run_client() :\n",
    "    conn, proto = await asyncio.get_running_loop().create_unix_connection \\\n",
    "      (\n",
    "        protocol_factory = NumberStream,\n",
    "        path = NUMBER_SERVER\n",
    "      )\n",
    "    for request in (12345, 54321, 98765) :\n",
    "        proto.send_number(request)\n",
    "        response = await proto.get_next_number()\n",
    "        sys.stderr.write(\"Client sent %d, got back %d from server\\n\" % (request, response))\n",
    "        await asyncio.sleep(2)\n",
    "    #end for\n",
    "    sys.stderr.write(\"client closing\\n\") # debug\n",
    "    conn.close()\n",
    "#end run_client\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, create the actual tasks, and give them a chance to run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "server_task = asyncio.create_task(run_server())\n",
    "client_task = asyncio.create_task(run_client())\n",
    "\n",
    "await asyncio.gather(server_task, client_task)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example Transport #\n",
    "\n",
    "Next, let us try implementing our own transport. Rather than bother with any actual network connection, we will create a simple in-memory pipe which just copies data between its two ends."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MemPipeTransport(asyncio.Transport) :\n",
    "    \"One end of a simple in-memory pipe transport. Do not instantiate\" \\\n",
    "    \" directly; use the create_pair() classmethod to create a linked pair\" \\\n",
    "    \" of these.\"\n",
    "\n",
    "    def __init__(self) :\n",
    "        self.protocol = None\n",
    "        self.peer = None\n",
    "    #end __init__\n",
    "\n",
    "    @classmethod\n",
    "    def create_pair(celf) :\n",
    "        \"creates a pair of MemPipeTransport objects such that anything\" \\\n",
    "        \" written to one can be read by the other.\"\n",
    "        pipe1 = celf()\n",
    "        pipe2 = celf()\n",
    "        pipe1.peer = pipe2\n",
    "        pipe2.peer = pipe1\n",
    "        return pipe1, pipe2\n",
    "    #end create_pair\n",
    "\n",
    "    # Minimal set of Transport methods implemented, just enough\n",
    "    # to get the example working:\n",
    "\n",
    "    def set_protocol(self, protocol) :\n",
    "        if self.protocol != None :\n",
    "            self.protocol.connection_lost(None)\n",
    "        #end if\n",
    "        self.protocol = protocol\n",
    "        protocol.connection_made(self)\n",
    "    #end set_protocol\n",
    "\n",
    "    def write(self, data) :\n",
    "        self.peer.protocol.data_received(data)\n",
    "    #end write\n",
    "\n",
    "    def close(self) :\n",
    "        if self.protocol != None :\n",
    "            self.protocol.eof_received()\n",
    "            self.protocol = None\n",
    "        #end if\n",
    "        if self.peer != None :\n",
    "            self.peer.peer = None # avoid endless recursion\n",
    "            self.peer.close()\n",
    "            self.peer = None\n",
    "        #end if\n",
    "    #end close\n",
    "\n",
    "#end MemPipeTransport"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are no generic `asyncio` methods for setting up connections using custom transports, so we will do all the transport and protocol setup ourselves."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "client_pipe, server_pipe = MemPipeTransport.create_pair()\n",
    "client_proto = NumberStream()\n",
    "server_proto = NumberStream()\n",
    "client_pipe.set_protocol(client_proto)\n",
    "server_pipe.set_protocol(server_proto)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are sample server and client tasks to make use of our custom transport connection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "async def run_pipe_server() :\n",
    "    while not server_proto.eof :\n",
    "        try :\n",
    "            num = await server_proto.get_next_number()\n",
    "        except StopAsyncIteration :\n",
    "            break\n",
    "        #end try\n",
    "        sys.stderr.write(\"Server got number %d, sending back %d\\n\" % (num, num + 1))\n",
    "        server_proto.send_number(num + 1)\n",
    "    #end while\n",
    "#end run_pipe_server\n",
    "\n",
    "async def run_pipe_client() :\n",
    "    for request in (12345, 54321, 98765) :\n",
    "        client_proto.send_number(request)\n",
    "        response = await client_proto.get_next_number()\n",
    "        sys.stderr.write(\"Client sent %d, got back %d from server\\n\" % (request, response))\n",
    "        await asyncio.sleep(2)\n",
    "    #end for\n",
    "    client_pipe.close()\n",
    "#end run_pipe_client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us run them and see what happens:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pipe_server_task = asyncio.create_task(run_pipe_server())\n",
    "pipe_client_task = asyncio.create_task(run_pipe_client())\n",
    "await asyncio.gather(pipe_server_task, pipe_client_task)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusion #\n",
    "\n",
    "Don’t think of the Transports+Protocols system as some way to freely “mix and match” different protocols with different transports. Things are not really that general. Bytestream-based protocols are going to assume some kind of reliable bytestream-based transport. And the standard calls for setting up connections assume transports based on POSIX sockets (or [pipes](https://docs.python.org/3/library/asyncio-protocol.html#subprocess-protocols)) anyway. You can step a bit beyond this if you do more work yourself. I think you are going to implement custom protocols more often than you are going to implement custom transports."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
