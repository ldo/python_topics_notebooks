/*
    Example shared library written to use to demonstrate how to call a
    variadic function written in C typesafely from Python using
    ctypes.

    Copyright 2024 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
    Licensed CC0 <https://creativecommons.org/publicdomain/zero/1.0/>;
    do with it what you will.
*/

typedef double
    real;

typedef enum
  {
    STACKCALC_END = 0, /* marks end of arglist */
    STACKCALC_PUSH = 1,
      /* followed by a real operand in the arglist, which is
        pushed on the operand stack */
  /* following are not followed by operands in the arglist;
    instead they expect two operands on the stack, which are
    popped, have the specified operation applied to them, and
    the result pushed back on the stack. */
    STACKCALC_ADD = 2,
    STACKCALC_SUB = 3,
    STACKCALC_MUL = 4,
    STACKCALC_DIV = 5,
  } stackcalc_op;

real stack_calc
  (
    stackcalc_op op, /* first operator */
    ...
      /* remaining operands (of type real)/operators (of type stackcalc_op),
        ending with STACKCALC_END */
  );
  /* starting with an empty operand stack, carries out the specified
    operations on the specified operands, at the end of which there
    should be just a single value on the operand stack. This value is
    popped and returned as the function result. */
