{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Uses For Metaclasses\n",
    "\n",
    "[**Metaclasses**](https://docs.python.org/3/reference/datamodel.html#metaclasses) seem like one of the more esoteric features of Python. They arise quite naturally from the Python data model:\n",
    "  * Every value in Python is an object.\n",
    "  * Every object is an instance of a class.\n",
    "  * Classes are also values, hence objects, that are generated and manipulated at runtime⸸;\n",
    "  * Therefore classes must also be instances of classes.\n",
    "  * The class that a class is an instance of is the *metaclass* of the class.\n",
    "  * The default metaclass is called [`type`](https://docs.python.org/3/library/functions.html#type) (which is one of 2 meanings of this built-in function). But you can specify your own.\n",
    "\n",
    "Would you ever write code that makes use of them? In fact, you likely already have, since they are a [key component](https://docs.python.org/3/library/enum.html#enum.EnumType) in how enumerated types are defined: where other languages might have `enum` types as a core language feature, in Python they are merely provided by a [library module](https://docs.python.org/3/library/enum.html), with no special magic in it at all. So every time you define an enumerated type using the factilities of this module, you are automatically taking advantage of its custom metaclass.\n",
    "\n",
    "A more pertinent question would be: would you ever create your own metaclass? This notebook will look in detail at one sort of situation where you might want to do so.\n",
    "\n",
    "A common feature of my example, and the `enum` one, is that **metaclasses tend to be most useful in conjunction with subclassing**. That is, the metaclass will typically only be explicitly attached to a base class, and then it automatically gets invoked to perform its special actions in the creation of subclasses of that base class. Maybe there are other usage scenarios which don’t fit this pattern; let me know if you find any.\n",
    "\n",
    "<SPAN STYLE=\"font-size: small\">⸸What, you thought a Python class definition was a _declaration_, like in other languages? No, it is better thought of as a form of assignment statement. The same applies to function definitions, by the way.</SPAN>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An Automatic Class-Specialization Hierarchy\n",
    "\n",
    "Python’s built-in [`OSError` class](https://docs.python.org/3/library/exceptions.html#OSError) represents exceptions raised for errors returned from operating system calls. This also has a [bunch of subclasses](https://docs.python.org/3/library/exceptions.html#os-exceptions), for common error codes, which are used where appropriate to represent a particular OS error code.\n",
    "\n",
    "These subclasses make handling these common errors more convenient, because then your `try`-block can have, for example, a specific `except FileNotFoundError` clause, instead of doing a more general `except OSError` and then having to check the `errno` field on the exception for the `ENOENT` code and re-raising the exception if it’s a code you don’t want to handle. Compare"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import errno\n",
    "\n",
    "NOSUCHFILE = \"/dev/no_such_file\" # hopefully guaranteed not to exist\n",
    "\n",
    "try :\n",
    "    f = open(NOSUCHFILE, \"r\")\n",
    "except OSError as err :\n",
    "    if err.errno != errno.ENOENT :\n",
    "        raise\n",
    "    #end if\n",
    "    print(\"No such file as “%s”\" % NOSUCHFILE)\n",
    "#end try"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try :\n",
    "    f = open(NOSUCHFILE, \"r\")\n",
    "except FileNotFoundError :\n",
    "    print(\"No such file as “%s”\" % NOSUCHFILE)\n",
    "#end try"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The subclass substitution happens automatically, and your own code can take advantage of it: just instantiate the `OSError` base class directly, and it will substitute the right subclass depending on the error code you pass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "errnames = dict((getattr(errno, n), n) for n in dir(errno) if n.startswith(\"E\"))\n",
    "\n",
    "for errcode in (errno.ENOENT, errno.ECONNABORTED, errno.ENAMETOOLONG) :\n",
    "    exc = OSError(errcode, \"dummy\")\n",
    "      # need at least second arg in order for this to work\n",
    "    print(\"%s(%d) => %s\" % (errnames[errcode], errcode, type(exc).__name__))\n",
    "#end for"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can imagine similar situations like this where you have a set of error codes from some API, and you want to define a hierarchy of custom exception classes to represent these, in a similarly convenient way. That is to day, instead of defining some custom function to choose which exception class to use based on the error code, let the exception base class itself figure it out when you try to instantiate it.\n",
    "\n",
    "Just for fun, let’s try defining some exceptions for reporting [telephone call hangup codes](https://docs.asterisk.org/Configuration/Miscellaneous/Hangup-Cause-Mappings/), as used with the Asterisk telephony engine.  We won’t do more than a few: just enough to illustrate the point.\n",
    "\n",
    "The basic idea is we will have a base class called `HangupCause` which will take a single instance attribute, the integer error code. Then we create subclasses of `HangupCause`, and in each of these, we define a `_CODES_` class variable which holds a set of error codes for which `HangupCause` will automatically instantiate this subclass instead of the base class. For example, after having set up the base class and metaclass, to define a special class for code 16 (normal hangup after a call is finished) would be as simple as\n",
    "\n",
    "```\n",
    "    class HangupNormalClearing(HangupCause) :\n",
    "        _CODES_ = {16}\n",
    "    #end HangupNormalClearing\n",
    "```\n",
    "\n",
    "Can’t be easier than that, can it?\n",
    "\n",
    "How shall we do this? In the `HangupCause` base class, we will have a class variable called `_SUBCLASSES_`, which is a dictionary mapping particular hangup codes to the corresponding subclass of itself to use instead of the base class. Then it will look at this in its `__new__()` method (which we use in lieu of an `__init__()` method because its gives us this degree of control):\n",
    "\n",
    "```\n",
    "    def __new__(cself, errcode) :\n",
    "        use_class = cself._SUBCLASSES_.get(errcode, cself)\n",
    "        inst = super().__new__(use_class)\n",
    "        inst.errcode = errcode\n",
    "        return inst\n",
    "    #end __new__\n",
    "```\n",
    "\n",
    "The metaclass, which I am calling `HangupCauseType`, will automatically update this `_SUBCLASSES_` dict in the base class every time a subclass is defined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class HangupCauseType(type) :\n",
    "\n",
    "    def __new__(cself, name, bases, namespace) :\n",
    "        result = type.__new__(cself, name, bases, dict(namespace))\n",
    "        if \"_CODES_\" in namespace :\n",
    "            for code in namespace[\"_CODES_\"] :\n",
    "                HangupCause._SUBCLASSES_[code] = result\n",
    "            #end for\n",
    "        #end if\n",
    "        return result\n",
    "    #end __new__\n",
    "\n",
    "#end HangupCauseType"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here is the full definition of the `HangupCause` base class, with a `__repr__()` method thrown in just to make the subsequent examples clearer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class HangupCause(Exception, metaclass = HangupCauseType) :\n",
    "\n",
    "    __slots__ = (\"errcode\",)\n",
    "    _SUBCLASSES_ = {}\n",
    "\n",
    "    def __new__(cself, errcode) :\n",
    "        use_class = cself._SUBCLASSES_.get(errcode, cself)\n",
    "        inst = super().__new__(use_class)\n",
    "        inst.errcode = errcode\n",
    "        return inst\n",
    "    #end __new__\n",
    "\n",
    "    def __repr__(self) :\n",
    "        return \"%s(%d)\" % (type(self).__name__, self.errcode)\n",
    "    #end __repr__\n",
    "    \n",
    "#end HangupCause"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, that’s not a large amount of code at all.\n",
    "\n",
    "Now for some example subclasses, defining some seemingly-related groupings of error codes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class HangupNormalClearing(HangupCause) :\n",
    "    _CODES_ = {16}\n",
    "#end HangupNormalClearing\n",
    "\n",
    "class HangupNetworkTrouble(HangupCause) :\n",
    "    _CODES_ = {2, 3, 38, 42}\n",
    "#end HangupNetworkTrouble"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now when I instantiate `HangupCause` with some codes, note the names of the classes it actually substitutes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HangupCause(16), HangupCause(3), HangupCause(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can even have subclasses of subclasses! For example, here is an arbitrary intermediate grouping called `NumberTrouble`, which in turn has its own subclasses listing particular error codes in this subcategory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class NumberTrouble(HangupCause) :\n",
    "    pass\n",
    "#end NumberTrouble\n",
    "\n",
    "class BadTrunkPrefix(NumberTrouble) :\n",
    "    _CODES_ = {5}\n",
    "#end BadTrunkPrefix\n",
    "\n",
    "class BadNumberFormat(NumberTrouble) :\n",
    "    _CODES_ = {28}\n",
    "#end BadNumberFormat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As before, we always instantiate the base `HangupCause` class directly, and it automatically dispatches to the right entry in our whole hierarchy of subclasses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for code in (5, 38, 28) :\n",
    "    exc = HangupCause(code)\n",
    "    print(str(exc), \"is NumberTrouble?\", isinstance(exc, NumberTrouble))\n",
    "#end for"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
