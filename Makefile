#+
# Build the C code associated with the Python Topics Notebooks.
#
# Copyright 2024 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed CC0 <https://creativecommons.org/publicdomain/zero/1.0/>;
# do with it what you will.
#-

CFLAGS=-g -fPIC -Wall -Wno-parentheses -Wno-switch

stack_calc.so : stack_calc.o
	$(CC) $^ -shared -o $@

stack_calc.o : stack_calc.c

clean :
	rm -f stack_calc.so stack_calc.o

.PHONY : clean
