{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ASTs And Other Python Language Services\n",
    "\n",
    "This Notebook will give an overview of just some of the modules available in the [Language Services](https://docs.python.org/3/library/language.html) section of the standard Python library, along with the [`inspect`](https://docs.python.org/3/library/inspect.html) module (which is in a different section). I haven’t found a use for all the facilities there; I will just mention the ones that I have, or that I could at least concoct reasonable-looking examples of their use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import inspect\n",
    "import ast\n",
    "import dis\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the warnings about the code-generation features: there are no guarantees that these will remain substantially unchanged from one CPython version to the next. So don’t rely too much on these details in important libraries or scripts.\n",
    "\n",
    "Example toy function to play with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mymax(a : int, b : int) -> int :\n",
    "    \"sample function to see what compilation and disassembly look like.\"\n",
    "    if a > b :\n",
    "        return a\n",
    "    else :\n",
    "        return b\n",
    "    #end if\n",
    "#end mymax\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `inspect` module has a `getsource()` function that will try to retrieve the source for some object, like the above function. This usually seems to work for objects in normal code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "funcsrc = inspect.getsource(mymax)\n",
    "print(funcsrc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The built-in [`compile()`](https://docs.python.org/3/library/functions.html#compile) function can not only create code objects, it can also produce just the intermediate AST form. So these two would be equivalent:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "syntax1 = ast.parse(funcsrc, filename = \"<sample>\", mode = \"exec\", type_comments = True)\n",
    "syntax2 = compile(funcsrc, filename = \"<sample>\", mode = \"exec\", flags = ast.PyCF_ONLY_AST | ast.PyCF_TYPE_COMMENTS)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"ast.parse() output = %s\" % ast.dump(syntax1))\n",
    "print()\n",
    "print(\"compile() output = %s\" % ast.dump(syntax2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(I won’t bother delving too deeply into those blobs of nodes.)\n",
    "\n",
    "The normal function of `compile()` is to produce a code object, and it can do this from source code or an AST:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "func = compile(syntax1, filename = \"<ast>\", mode = \"exec\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that a code object is not a function, and cannot be called as one:\n",
    "\n",
    "    print(\"max(%d, %d) = %d\\n\" % (3, 4, func(3, 4))) # won’t work\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can disassemble the byte code, to see what the compiler has produced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dis.dis(func)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the disassembly splits naturally into two parts: the part at the bottom is the actual function code, while the part at the top actually _defines_ the function, or alternatively _creates the function object_.\n",
    "\n",
    "I can execute this code object using `exec()`, to create a function object inside a specified namespace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "namespace = {}\n",
    "exec(func, namespace)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "namespace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, `namespace` has a function I can call:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"%s(%d, %d) = %d\" % (namespace[\"mymax\"].__name__, 3, 4, namespace[\"mymax\"](3, 4)))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, along the way, the source code got lost:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try :\n",
    "    print(\"source of mymax function = %s\" % repr(inspect.getsource(namespace[\"mymax\"])))\n",
    "except Exception as err :\n",
    "    print(\"inspecting mymax function: %s\" % repr(err))\n",
    "#end try\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the function object includes a pointer to the code object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dis.dis(namespace[\"mymax\"].__code__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doing a second `exec()` on the same code object produces a new function object, but it points to the same coce object as the first one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ns2 = {}\n",
    "exec(func, ns2)\n",
    "ns2[\"mymax\"] is namespace[\"mymax\"], ns2[\"mymax\"].__code__ is namespace[\"mymax\"].__code__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
